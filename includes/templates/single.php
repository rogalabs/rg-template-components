<?php
wp_head();
while (have_posts()) : the_post(); ?>
    <div class="thrv_wrapper">
        <?php the_content(); ?>
    </div><?php
endwhile;
wp_footer();
