<?php
/*
Plugin Name: Roga Template Components
Plugin URI: https://speakout.123host.net.au/
Description: Build reusable template components. Requires Thrive Architect Plugin
Version: 1.0.0
Author: Siderlan Santos
Author URI: sider@rogalabs.com
License: GPL2
Requires: Thrive Architect
 */

/**
 * Rg_Template_Components class
 */
class Rg_Template_Components
{
  private static $instance = null;

  private $capabilities = array(
    'edit_post' => 'edit_rg_template_component',
    'read_post' => 'read_rg_template_component',
    'delete_post' => 'delete_rg_template_component',
    'edit_posts' => 'edit_rg_template_components',
    'edit_others_posts' => 'edit_others_rg_template_components',
    'publish_posts' => 'publish_rg_template_components',
    'read_private_posts' => 'read_private_rg_template_components',
  );

  private function __construct()
  {
    $this->setup_hooks();
  }

  public static function get_instance()
  {
    if (static::$instance == null) {
      static::$instance = new static;
    }

    return static::$instance;
  }

  public function activate()
  {
    $this->register_post_type();
    $this->setup_capabilities();
    flush_rewrite_rules();
  }

  public function deactivate()
  {
    flush_rewrite_rules();
  }

  public function setup_hooks()
  {
    add_action('init', array($this, 'register_post_type'), 0);
    add_filter('template_include', array($this, 'include_template'), 999);
  }

  public function setup_capabilities()
  {
    $role = get_role('administrator');
    if ($role) {
      foreach ($this->capabilities as $cap)
      $role->add_cap($cap);
    }
  }

  public function register_post_type()
  {
    $labels = array(
      'name' => _x('Components', 'Post Type General Name', 'rg_components'),
      'singular_name' => _x('Component', 'Post Type Singular Name', 'rg_components'),
      'menu_name' => __('Template Components', 'rg_components'),
      'name_admin_bar' => __('Template Component', 'rg_components'),
      'archives' => __('Component Archives', 'rg_components'),
      'attributes' => __('Component Attributes', 'rg_components'),
      'parent_item_colon' => __('Parent Component:', 'rg_components'),
      'all_items' => __('All Components', 'rg_components'),
      'add_new_item' => __('Add New Component', 'rg_components'),
      'add_new' => __('Add New', 'rg_components'),
      'new_item' => __('New Component', 'rg_components'),
      'edit_item' => __('Edit Component', 'rg_components'),
      'update_item' => __('Update Component', 'rg_components'),
      'view_item' => __('View Component', 'rg_components'),
      'view_items' => __('View Components', 'rg_components'),
      'search_items' => __('Search Component', 'rg_components'),
      'not_found' => __('Not found', 'rg_components'),
      'not_found_in_trash' => __('Not found in Trash', 'rg_components'),
      'featured_image' => __('Featured Image', 'rg_components'),
      'set_featured_image' => __('Set featured image', 'rg_components'),
      'remove_featured_image' => __('Remove featured image', 'rg_components'),
      'use_featured_image' => __('Use as featured image', 'rg_components'),
      'insert_into_item' => __('Insert into item', 'rg_components'),
      'uploaded_to_this_item' => __('Uploaded to this item', 'rg_components'),
      'items_list' => __('Components list', 'rg_components'),
      'items_list_navigation' => __('Components list navigation', 'rg_components'),
      'filter_items_list' => __('Filter components list', 'rg_components'),
    );

    $args = array(
      'label' => __('Component', 'rg_components'),
      'description' => __('Post type used to define template components', 'rg_components'),
      'labels' => $labels,
      'supports' => array('title', 'revisions' ),
      'taxonomies' => array(),
      'hierarchical' => true,
      'public' => true,
      'show_ui' => true,
      'show_in_menu' => true,
      'menu_icon' => 'dashicons-tagcloud',
      'show_in_admin_bar' => true,
      'show_in_nav_menus' => false,
      'can_export' => true,
      'has_archive' => false,
      'exclude_from_search' => true,
      'publicly_queryable' => true,
      'capabilities' => $this->capabilities,
      'show_in_rest' => false,
    );

    register_post_type('rg-component', $args);
  }

  public function include_template($template) {
    if (is_singular( 'rg-component' )) {
      $template = plugin_dir_path(__FILE__) . 'includes/templates/single.php';
    }

    return $template;
  }
}

/**
 * Undocumented function
 *
 * @return void
 */
function rg_template_components_activate()
{
  $rgtc = Rg_Template_Components::get_instance();
  $rgtc->activate();
}

/**
 * Undocumented function
 *
 * @return void
 */
function rg_template_components_deactivate()
{
  $rgtc = Rg_Template_Components::get_instance();
  $rgtc->deactivate();
}

global $rg_template_components;
$rg_template_components = Rg_Template_Components::get_instance();

register_activation_hook(__FILE__, 'rg_template_components_activate');
register_deactivation_hook(__FILE__, 'rg_template_components_deactivate');
